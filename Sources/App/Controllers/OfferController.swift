//
//  OfferController.swift
//  App
//
//  Created by Kasper Welner on 05/10/2017.
//

import Foundation
import Vapor
import HTTP
import AuthProvider
import Routing

final class OfferController: ResourceRepresentable {
    
    func makeResource() -> Resource<Offer> {
        return Resource(index:index,
                        store:store,
                        show:show,
                        destroy:destroy)
    }
    
    ///GET /offers
    ///Lists all offers for the current user's company, or all offers if sysadmin
    func index(req: Request) throws -> ResponseRepresentable {
        let user = try req.user()
        switch try user.accessLevel() {
        case .user, .owner:
            return try Offer.makeQuery().filter("company", user.companyId).all().makeJSON()
        case .sysadmin:
            return try Offer.all().makeJSON()
        }
    }
    
    ///GET /offers/:id
    ///Lists all offers for the current user's company, or all offers if sysadmin
    func show(req: Request, offer: Offer) throws -> ResponseRepresentable {
        let user = try req.user()
        guard try user.canView(companyWithId: offer.companyId) else { throw Abort(.forbidden, reason: "You don't have the rights to view this offer") }
        var offerJSON = try offer.makeJSON()
        offerJSON["employeeTimes"] = try offer.employeeTimes.all().map{ try $0.unwrapJSON() }.makeJSON()
        offerJSON["expenses"] = try offer.expenses.all().makeJSON()
        return offerJSON
    }
    
    ///POST /offers/:id
    ///Creates a new offer
    ///Optional: totalTenderAmountOverride (float)
    ///Optional: companyId
    ///Optional: employeeHours [
    /// {
    ///     "employeeId" : 1,
    ///     "days" : 1.5
    /// }...
    ///]
    ///Optional: expenses [
    /// {
    ///     "title" : "Fejning for egen dør",
    ///     "cost" : 95
    /// }...
    ///]
    func store(req: Request) throws -> ResponseRepresentable {
        let user = try req.user()
        var companyId:Identifier
        
        switch try user.accessLevel() {
        case .sysadmin:
            guard let id = req.data["companyId"]?.int else { throw Abort(.preconditionFailed, reason: "companyId must be supplied when sysAdmin") }
            companyId = Identifier(id)
        case .owner, .user:
            guard let userCompanyId = user.companyId else { throw Abort(.internalServerError, reason: "companyId on user/owner is nil") }
            companyId = userCompanyId
        }
        
        guard let userId = user.id else { throw Abort(.internalServerError) }
        
        guard try Company.find(companyId)?.exists ?? false else { throw Abort(.badRequest, reason:"Company with ID \(companyId.string ?? "-") not found") }
        
        let offer = Offer(userId: userId,
                          companyId: companyId,
                          totalTenderAmountOverride: req.data["totalTenderAmountOverride"]?.double)
        try offer.save()
        
        guard let offerId = offer.id else { throw Abort.serverError }
        
        do {
            let hoursJson = try req.JSONData()["employeeHours"]
            try hoursJson?.array?.forEach { json in
                guard let employeeId = json["employeeId"]?.int else { throw Abort(.badRequest, reason: "Missing employeeId") }
                guard let days = json["days"]?.double else { throw Abort(.badRequest, reason: "Missing days") }
                guard let employee = try Employee.find(employeeId), let empIdentifier = employee.id else { throw Abort(.badRequest, reason: "No employee for id \(employeeId)") }
                let employeeTime = EmployeeTime.init(days: days, employeeId: empIdentifier, offerId: offerId)
                try employeeTime.save()
            }
            
        } catch {
            throw Abort(.badRequest, reason: (error as? Abort)?.reason ?? "Malformed 'employeeHours' array \(error)")
        }
        
        do {
            let expensesJson = try req.JSONData()["expenses"]
            try expensesJson?.array?.forEach { json in
                guard let title = json["title"]?.string else { throw Abort.badRequest }
                guard let cost = json["cost"]?.int else { throw Abort.badRequest }
                
                let expense = Expense(title: title, cost: cost, offerId: offerId)
                try expense.save()
            }
            
        } catch {
            throw Abort(.badRequest, reason: (error as? Abort)?.reason ?? "Malformed 'expenses array")
        }
        
        var offerJSON = try offer.makeJSON()
        offerJSON["employeeTimes"] = try offer.employeeTimes.all().map{ try $0.unwrapJSON() }.makeJSON()
        offerJSON["expenses"] = try offer.expenses.all().makeJSON()
        return offerJSON
    }
    
    ///DELETE /offers/:id
    ///Deletes the offer with the provided id
    func destroy(req: Request, offer: Offer) throws -> ResponseRepresentable {
        let user = try req.user()
        guard try user.canView(companyWithId: offer.companyId) else { throw Abort(.forbidden, reason: "You don't have the rights to view this offer") }
        try offer.delete()
        return Response(status: .ok)
    }
}
