//
//  UserController.swift
//  App
//
//  Created by Kasper Welner on 05/10/2017.
//

import Foundation
import Vapor
import HTTP
import AuthProvider
import Routing
import Validation

final class UserController: ResourceRepresentable {
  
    let authed:Bool
    let droplet:Droplet
    
    init(authed:Bool, droplet:Droplet) {
        self.authed = authed
        self.droplet = droplet
    }
    
    func makeResource() -> Resource<User> {
        switch authed {
        case false:
            return Resource(
                store:store
            )
        case true:
            return Resource(
                index:index,
                show:show,
                update:update,
                destroy:destroy
            )
        }
    }
    
    /// create a new user
    ///
    /// POST /users
    /// Possible errors:
    /// - 412 A user with that email already exists
    /// - 412 Invalid invitation code
    /// - 412 Missing parameters
    func store(_ req: Request) throws -> ResponseRepresentable {
        
        let json = try req.JSONData()
        
        // initialize the name and email from
        // the request json
        let user = try User(json: json)
        
        // ensure no user with this email already exists
        guard try User.makeQuery().filter(User.DatabaseKeys.email, user.email).first() == nil else {
            throw Abort(.preconditionFailed, reason: "A user with that email already exists.")
        }
        
        do {
            try EmailValidator().validate(user.email)
        } catch {
            //412
            throw Abort(.preconditionFailed, reason:"The email address does not have a valid format")
        }
        
        // require a plaintext password is supplied
        guard let password = json["password"]?.string else {
            throw Abort(.preconditionFailed)
        }
        
        // hash the password and set it on the user
        user.password = try droplet.hash.make(password.makeBytes()).makeString()
        
        guard let invitationCode = json["invitationCode"] else { throw Abort(.preconditionFailed, reason: "Missing 'invitationCode' field") }
        
        if let company = try Company.makeQuery().filter(Company.DatabaseKeys.ownerInvitationCode, invitationCode).first(), let companyId = company.id {
            user.set(accessLevel: .owner)
            user.companyId = companyId
        } else if let company = try Company.makeQuery().filter(Company.DatabaseKeys.userInvitationCode, invitationCode).first(), let companyId = company.id {
            user.set(accessLevel: .user)
            user.companyId = companyId
        } else if invitationCode.string == "ADMIN_A_F" {
            user.set(accessLevel: .sysadmin)
        } else {
            throw Abort(.preconditionFailed, reason: "Invalid Invitation Code")
        }
        
        // save and return the new user
        try user.save()
        let token = try Token.generate(for: user)
        try token.save()
        
        return try JSON(node: ["user" : user.makeJSON(), "token" : token.token])
    }
    
    /// GET /users
    func index(req:Request) throws -> ResponseRepresentable {
        if try req.user().accessLevel().isSysadmin {
            return try User.all().makeJSON()
        }
        guard try req.user().accessLevel().isOwner, let companyId = try req.user().companyId else { throw Abort(Status.forbidden, reason: "You need to be owner or sysadmin to get the list of users") }
        return try User.makeQuery().filter(User.DatabaseKeys.companyId, companyId).all().makeJSON()
    }
    
    /// GET /users/:id
    func show(req:Request, user:User) throws -> ResponseRepresentable {
        return try user.makeJSON()
    }
    
    /// PATCH /users/:id
    /// Fails if :id doesn't correspond to user's id
    func update(req:Request, user:User) throws -> ResponseRepresentable {
        guard try req.user().id == user.id else { throw Abort(.forbidden) }
        try user.update(data: try req.JSONData())
        return try user.makeJSON()
    }
    
    /// DELETE /users/:id
    /// Fails if authorized user isn't sysadmin
    func destroy(req:Request, user:User) throws -> ResponseRepresentable {
        guard try req.user().accessLevel().isSysadmin || req.user().id == user.id || (try req.user().accessLevel().isOwner && req.user().companyId == user.companyId) else { throw Abort(Status.forbidden, reason: "Only sysadmin, company owner or user himself can delete this user") }
        try Token.makeQuery().filter("userId", in: [user.id]).delete()
        try user.delete()
        return Response(status: .ok)
    }
}

