//
//  EmployeeController.swift
//  App
//
//  Created by Kasper Welner on 05/10/2017.
//

import Foundation
import Vapor
import HTTP
import AuthProvider
import Routing

final class EmployeeController: ResourceRepresentable {
    
    func makeResource() -> Resource<Employee> {
        return Resource(index:index,
                        store:store,
                        show:show,
                        update:update,
                        destroy:destroy)
    }
    
    /// Returns all companies
    /// Optional: companyId
    /// GET /employees
    func index(_ req: Request) throws -> ResponseRepresentable {
        let user = try req.user()
        guard let companyId = req.data["companyId"]?.int ?? user.companyId?.int else {
            guard try user.accessLevel().isSysadmin else { throw Abort(.internalServerError, reason: "Authenticated user has no company and is not sysadmin") }
            return try Employee.makeQuery().all().makeJSON()
        }
        guard try user.accessLevel().isSysadmin || companyId == user.companyId?.int else { throw Abort(.forbidden, reason: "These employees do not belong to you") }
        return try Employee.makeQuery().filter("companyId", companyId).sort("id", .ascending).all().makeJSON()
    }
    
    /// Creates a new Employee
    /// Requires: firstName, lastName, title, salary, hoursPerWeek, pensionStartDate
    /// Optional: pensionCustomPercent, companyId
    ///
    /// POST /employees
    func store(_ req: Request) throws -> ResponseRepresentable {
        let user = try req.user()
        var companyId:Int
        if let companyIdParam = req.data["companyId"]?.int, companyIdParam != user.companyId?.int {
            guard try user.accessLevel() == .sysadmin else {
                throw Abort(.forbidden)
            }
            companyId = companyIdParam
        } else {
            if try user.accessLevel().isSysadmin { throw Abort(.preconditionFailed, reason:"Please supply a companyId when trying to create a user as a sysadmin") }
            guard try user.accessLevel().isOwner else { throw Abort(.forbidden, reason:"Only owners of this company can create or modify users") }
            guard let userCompanyId = user.companyId?.int else { throw Abort(.internalServerError) }
            companyId = userCompanyId
        }
        
        let employee = try Employee(json: req.JSONData(), creator: user, companyId: companyId)
        try employee.save()
        return employee
    }
    
    
    /// GET /employees/:id
    func show(_ req: Request, employee:Employee) throws -> ResponseRepresentable {
        let user = try req.user()
        if try user.accessLevel() == .sysadmin || user.companyId == employee.companyId {
            return try employee.makeJSON()
        }
        
        throw Abort(.forbidden)
    }
    
    /// DELETE /employees/:id
    func destroy(_ req: Request, employee:Employee) throws -> ResponseRepresentable {
        let user = try req.user()
        guard try user.accessLevel().isSysadmin || (user.accessLevel().isOwner && employee.companyId == user.companyId) else { throw Abort(Status.forbidden, reason: "Only owners of this company can delete employees") }
        try employee.employeeTimes.delete()
        try employee.delete()
        return Response(status: .ok)
    }
    
    /// PATCH /employees/:id
    /// Optional: firstName, lastName, title, salary, hoursPerWeek, pensionStartDate, pensionCustomPercent
    func update(req:Request, employee:Employee) throws -> ResponseRepresentable {
        let user = try req.user()
        guard try user.accessLevel().isSysadmin || (user.accessLevel().isOwner && employee.companyId == user.companyId) else { throw Abort(Status.forbidden, reason: "Only owners of this company can modify employees") }
        try employee.update(json: try req.JSONData())
        return try employee.makeJSON()
    }
}
