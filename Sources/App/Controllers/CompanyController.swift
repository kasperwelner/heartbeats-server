//
//  CompanyController.swift
//  App
//
//  Created by Kasper Welner on 08/10/2017.
//

import Foundation
import Vapor
import HTTP
import AuthProvider
import Routing

final class CompanyController: ResourceRepresentable {
    
    func makeResource() -> Resource<Company> {
        return Resource(index:index,
                        store:store,
                        show:show,
                        destroy:destroy)
    }
    
    /// Creates a new company
    /// Requires: name, ownerInvitationCode, userInvitationCode
    ///
    /// POST /companies
    func store(_ req: Request) throws -> ResponseRepresentable {
        let user = try req.user()
        guard try user.accessLevel() == .sysadmin else {
            throw Abort(.forbidden)
        }
        
        let company = try Company(json: try req.JSONData())
        try company.save()
        return company
    }
    
    /// Returns all companies
    /// GET /companies
    func index(_ req: Request) throws -> ResponseRepresentable {
        let user = try req.user()
        guard try user.accessLevel().isSysadmin else {
            throw Abort(.forbidden, reason: "Only sysadmins can view full list of companies")
        }
        
        return try Company.makeQuery().all().makeJSON()
    }
    
    /// GET /companies/:id
    func show(_ req: Request, company:Company) throws -> ResponseRepresentable {
        let user = try req.user()
        if try user.accessLevel() == .sysadmin || user.companyId == company.id {
            return try company.makeJSONWithUsers()
        }
        
        throw Abort(.forbidden)
    }
    
    /// DELETE /companies/:id
    func destroy(_ req: Request, company:Company) throws -> ResponseRepresentable {
        let user = try req.user()
        guard try user.accessLevel() == .sysadmin else { throw Abort(Status.forbidden, reason: "Only sysadmins can delete companies") }
        try company.delete()
        return Response(status: .ok)
    }
}
