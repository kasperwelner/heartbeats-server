import Vapor
import FluentProvider
import AuthProvider
import HTTP

// sourcery: model
final class User: Model, SoftDeletable {
    
    enum AccessLevel : String {
        case sysadmin
        case owner
        case user
        
        var isSysadmin:Bool { return self == .sysadmin }
        var isOwner:Bool { return self == .owner }
        var isUser:Bool { return self == .user }
    }
    
    /// The name of the user
    var name: String
    
    /// The user's email
    var email: String
    
    /// The user's _hashed_ password
    //sourcery: ignoreJSONConvertible = true
    var password: String?
    
    //sourcery: preparation = foreignId, foreignTable = Company, unique = false
    var companyId: Identifier?
    
    /// Access Level
    //sourcery: jsonKey = accessLevel
    var accessLevelString: String?
    
// sourcery:inline:auto:User.Models
    let storage = Storage()

    internal init(
        name: String,
        email: String,
        password: String? = nil,
        companyId: Identifier? = nil,
        accessLevelString: String? = nil
    ) {
        self.name = name
        self.email = email
        self.password = password
        self.companyId = companyId
        self.accessLevelString = accessLevelString
    }
// sourcery:end
}

extension User {
    internal func makeJSONWithCompany() throws -> JSON {
        var json = try makeJSON()
        if try !self.accessLevel().isSysadmin {
            try json.set("company", company.get()?.makeJSON())
        }
        
        return json
    }
}
extension User {
    
    //sourcery: jsonKey = accessLevel
    func accessLevel() throws -> AccessLevel {
        guard let levelString = accessLevelString else { throw Abort.init(.internalServerError, reason: "\(name) does not have a valid access level") }
        guard let level = AccessLevel(rawValue: levelString) else { throw Abort.init(.internalServerError, reason: "\(levelString) is not a valid access level") }
        return level
    }
    
    func set(accessLevel:AccessLevel) {
        accessLevelString = accessLevel.rawValue
    }
    
    var orders:Children<User, Offer> {
        return children()
    }
    
    var company:Parent<User, Company> {
        return parent(id: companyId)
    }
}

extension User {
    func owns(offer:Offer) -> Bool {
        return offer.userId == self.id
    }
    
    func owns(company:Company) throws -> Bool {
        switch (try self.accessLevel(), self.companyId) {
        case (.sysadmin, _):
            return true
        case (.owner, let companyId):
            return companyId == company.id
        case (.user, _):
            return false
        }
    }
    
    func canView(companyWithId companyId:Identifier) throws -> Bool {
        switch try self.accessLevel() {
        case .sysadmin:
            return true
        case .owner, .user:
            return companyId == self.companyId
        }
    }
}

extension User {
    func update(data:JSON) throws {
        if let name = data["name"]?.string {
            self.name = name
        }
        
        if let email = data["email"]?.string {
            self.email = email
        }
        
        try self.save()
    }
}

// MARK: Password

// This allows the User to be authenticated
// with a password. We will use this to initially
// login the user so that we can generate a token.
extension User: PasswordAuthenticatable {
    var hashedPassword: String? {
        return password
    }
    
    public static var passwordVerifier: PasswordVerifier? {
        get { return _userPasswordVerifier }
        set { _userPasswordVerifier = newValue }
    }
}

// store private variable since storage in extensions
// is not yet allowed in Swift
private var _userPasswordVerifier: PasswordVerifier? = nil

// MARK: Request

extension Request {
    /// Convenience on request for accessing
    /// this user type.
    /// Simply call `let user = try req.user()`.
    func user() throws -> User {
        return try auth.assertAuthenticated()
    }
}

// MARK: Token

// This allows the User to be authenticated
// with an access token.
extension User: TokenAuthenticatable {
    typealias TokenType = Token
}
