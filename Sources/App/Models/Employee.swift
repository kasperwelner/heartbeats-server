//
//  Employee.swift
//  App
//
//  Created by Kasper Welner on 07/10/2017.
//

import Foundation
import Vapor
import FluentProvider

// sourcery: model
final class Employee: Model, Timestampable {
    var firstName: String
    var lastName: String
    var title: String
    var monthlySalary: Int
    var hoursPerWeek: Double
    var pensionStartDate: Date
    var pensionCustomPercent: Double?
    var hasPension: Bool
    
// sourcery: preparation    = foreignId
// sourcery: foreignTable   = Company
// sourcery: unique         = false
    let companyId: Identifier

// sourcery: preparation    = foreignId, foreignTable = User, unique = false
    let userId: Identifier
    
// sourcery:inline:auto:Employee.Models
    let storage = Storage()

    internal init(
        firstName: String,
        lastName: String,
        title: String,
        monthlySalary: Int,
        hoursPerWeek: Double,
        pensionStartDate: Date,
        pensionCustomPercent: Double? = nil,
        hasPension: Bool,
        companyId: Identifier,
        userId: Identifier
    ) {
        self.firstName = firstName
        self.lastName = lastName
        self.title = title
        self.monthlySalary = monthlySalary
        self.hoursPerWeek = hoursPerWeek
        self.pensionStartDate = pensionStartDate
        self.pensionCustomPercent = pensionCustomPercent
        self.hasPension = hasPension
        self.companyId = companyId
        self.userId = userId
    }
// sourcery:end
}
extension Employee {
    internal convenience init(json: JSON, creator:User, companyId:Int) throws {
        guard let userId = creator.id else { throw Abort(.internalServerError, reason: "User has no id") }
        guard try Company.find(companyId) != nil else { throw Abort(.badRequest, reason:"Company with ID \(companyId) not found") }
        var mJSON = json
        try mJSON.set("userId", userId)
        try self.init(json:mJSON)
    }
}

extension Employee {
    var employeeTimes: Children<Employee, EmployeeTime> {
        return children()
    }
    
    var company: Parent<Employee, Company> {
        return parent(id: companyId)
    }
    
    var creator: Parent<Employee, User> {
        return parent(id: userId)
    }
}

extension Employee {
    /// Optional: firstName, lastName, title, salary, hoursPerWeek, pensionStartDate, pensionCustomPercent
    func update(json:JSON) throws {
        self.firstName            = try json.get(JSONKeys.firstName.rawValue)
        self.lastName             = try json.get(JSONKeys.lastName.rawValue)
        self.title                = try json.get(JSONKeys.title.rawValue)
        self.monthlySalary        = try json.get(JSONKeys.monthlySalary.rawValue)
        self.hoursPerWeek        = try json.get(JSONKeys.hoursPerWeek.rawValue)
        self.pensionStartDate     = try json.get(JSONKeys.pensionStartDate.rawValue)
        self.pensionCustomPercent = try json.get(JSONKeys.pensionCustomPercent.rawValue)
        try self.save()
    }
}
