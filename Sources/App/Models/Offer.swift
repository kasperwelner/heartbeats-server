//
//  Order.swift
//  App
//
//  Created by Kasper Welner on 07/10/2017.
//

import Foundation
import Vapor
import FluentProvider

// sourcery: model
final class Offer: Model, Timestampable {

// sourcery: preparation    = foreignId
// sourcery: foreignTable   = User
// sourcery: unique         = false
    let userId:Identifier

// sourcery: preparation    = foreignId
// sourcery: foreignTable   = Company
// sourcery: unique         = false
    let companyId:Identifier
    
    var totalTenderAmountOverride:Double? = nil

// sourcery:inline:auto:Offer.Models
    let storage = Storage()

    internal init(
        userId: Identifier,
        companyId: Identifier,
        totalTenderAmountOverride: Double? = nil
    ) {
        self.userId = userId
        self.companyId = companyId
        self.totalTenderAmountOverride = totalTenderAmountOverride
    }
// sourcery:end
}

extension Offer {
    var expenses: Children<Offer, Expense> {
        return children()
    }
    
    var employeeTimes: Children<Offer, EmployeeTime> {
        return children()
    }
    
    var creator:Parent<Offer, User> {
        return parent(id: userId)
    }
}
