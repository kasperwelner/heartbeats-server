// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Company: JSONConvertible {
    internal enum JSONKeys: String {
        case name
        case ownerInvitationCode
        case userInvitationCode
    }

    // MARK: - JSONConvertible (Company)
    internal convenience init(json: JSON) throws {
        try self.init(
            name: json.get(JSONKeys.name.rawValue),
            ownerInvitationCode: json.get(JSONKeys.ownerInvitationCode.rawValue),
            userInvitationCode: json.get(JSONKeys.userInvitationCode.rawValue)
        )
    }

    internal func makeJSON() throws -> JSON {
        var json = JSON()

        try json.set(Company.idKey, id)
        try json.set(JSONKeys.name.rawValue, name)
        try json.set(JSONKeys.ownerInvitationCode.rawValue, ownerInvitationCode)
        try json.set(JSONKeys.userInvitationCode.rawValue, userInvitationCode)

        return json
    }
}

extension Company: ResponseRepresentable {}
