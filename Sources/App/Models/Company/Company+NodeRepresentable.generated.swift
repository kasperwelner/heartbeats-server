// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Company: NodeRepresentable {
    internal enum NodeKeys: String {
        case name
        case ownerInvitationCode
        case userInvitationCode
    }

    // MARK: - NodeRepresentable (Company)
    func makeNode(in context: Context?) throws -> Node {
        var node = Node([:])

        try node.set(Company.idKey, id)
        try node.set(NodeKeys.name.rawValue, name)
        try node.set(NodeKeys.ownerInvitationCode.rawValue, ownerInvitationCode)
        try node.set(NodeKeys.userInvitationCode.rawValue, userInvitationCode)

        return node
    }
}
