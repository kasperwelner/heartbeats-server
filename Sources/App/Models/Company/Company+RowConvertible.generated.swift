// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Company: RowConvertible {
    // MARK: - RowConvertible (Company)
    convenience internal init (row: Row) throws {
        try self.init(
            name: row.get(DatabaseKeys.name),
            ownerInvitationCode: row.get(DatabaseKeys.ownerInvitationCode),
            userInvitationCode: row.get(DatabaseKeys.userInvitationCode)
        )
    }

    internal func makeRow() throws -> Row {
        var row = Row()

        try row.set(DatabaseKeys.name, name)
        try row.set(DatabaseKeys.ownerInvitationCode, ownerInvitationCode)
        try row.set(DatabaseKeys.userInvitationCode, userInvitationCode)

        return row
    }
}
