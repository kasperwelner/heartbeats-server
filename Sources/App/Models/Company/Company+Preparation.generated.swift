// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Company: Preparation {
    internal enum DatabaseKeys {
        static let id = Company.idKey
        static let name = "name"
        static let ownerInvitationCode = "ownerInvitationCode"
        static let userInvitationCode = "userInvitationCode"
    }

    // MARK: - Preparations (Company)
    internal static func prepare(_ database: Database) throws {
        try database.create(self) {
            $0.id()
            $0.string(DatabaseKeys.name, unique: true)
            $0.string(DatabaseKeys.ownerInvitationCode, unique: true)
            $0.string(DatabaseKeys.userInvitationCode, unique: true)
        }

    }

    internal static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}
