// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension User: Preparation {
    internal enum DatabaseKeys {
        static let id = User.idKey
        static let name = "name"
        static let email = "email"
        static let password = "password"
        static let companyId = "companyId"
        static let accessLevelString = "accessLevelString"
    }

    // MARK: - Preparations (User)
    internal static func prepare(_ database: Database) throws {
        try database.create(self) {
            $0.id()
            $0.string(DatabaseKeys.name)
            $0.string(DatabaseKeys.email)
            $0.string(DatabaseKeys.password, optional: true)
            $0.foreignId(for: Company.self, optional: true, unique: false, foreignIdKey: DatabaseKeys.companyId)
            $0.string(DatabaseKeys.accessLevelString, optional: true)
        }

    }

    internal static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}
