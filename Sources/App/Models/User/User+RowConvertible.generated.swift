// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension User: RowConvertible {
    // MARK: - RowConvertible (User)
    convenience internal init (row: Row) throws {
        try self.init(
            name: row.get(DatabaseKeys.name),
            email: row.get(DatabaseKeys.email),
            password: row.get(DatabaseKeys.password),
            companyId: row.get(DatabaseKeys.companyId),
            accessLevelString: row.get(DatabaseKeys.accessLevelString)
        )
    }

    internal func makeRow() throws -> Row {
        var row = Row()

        try row.set(DatabaseKeys.name, name)
        try row.set(DatabaseKeys.email, email)
        try row.set(DatabaseKeys.password, password)
        try row.set(DatabaseKeys.companyId, companyId)
        try row.set(DatabaseKeys.accessLevelString, accessLevelString)

        return row
    }
}
