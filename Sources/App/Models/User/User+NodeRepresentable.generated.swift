// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension User: NodeRepresentable {
    internal enum NodeKeys: String {
        case name
        case email
        case password
        case companyId
        case accessLevelString
    }

    // MARK: - NodeRepresentable (User)
    func makeNode(in context: Context?) throws -> Node {
        var node = Node([:])

        try node.set(User.idKey, id)
        try node.set(NodeKeys.name.rawValue, name)
        try node.set(NodeKeys.email.rawValue, email)
        try node.set(NodeKeys.password.rawValue, password)
        try node.set(NodeKeys.companyId.rawValue, companyId)
        try node.set(NodeKeys.accessLevelString.rawValue, accessLevelString)

        return node
    }
}
