// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension User: JSONConvertible {
    internal enum JSONKeys: String {
        case name
        case email
        case companyId
        case accessLevel
    }

    // MARK: - JSONConvertible (User)
    internal convenience init(json: JSON) throws {
        try self.init(
            name: json.get(JSONKeys.name.rawValue),
            email: json.get(JSONKeys.email.rawValue),
            companyId: json.get(JSONKeys.companyId.rawValue),
            accessLevelString: json.get(JSONKeys.accessLevel.rawValue)
        )
    }

    internal func makeJSON() throws -> JSON {
        var json = JSON()

        try json.set(User.idKey, id)
        try json.set(JSONKeys.name.rawValue, name)
        try json.set(JSONKeys.email.rawValue, email)
        try json.set(JSONKeys.companyId.rawValue, companyId)
        try json.set(JSONKeys.accessLevel.rawValue, accessLevelString)

        return json
    }
}

extension User: ResponseRepresentable {}
