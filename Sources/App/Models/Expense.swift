//
//  Expense.swift
//  App
//
//  Created by Kasper Welner on 07/10/2017.
//

import Foundation
import Vapor
import FluentProvider

// sourcery: model
final class Expense: Model {
    let title: String
    var cost: Int

// sourcery: preparation    = foreignId
// sourcery: foreignTable   = Offer
// sourcery: unique         = false
    let offerId:Identifier
    
// sourcery:inline:auto:Expense.Models
    let storage = Storage()

    internal init(
        title: String,
        cost: Int,
        offerId: Identifier
    ) {
        self.title = title
        self.cost = cost
        self.offerId = offerId
    }
// sourcery:end
}

extension Expense {
    var order:Parent<Expense, Offer> {
        return parent(id: offerId)
    }
}
