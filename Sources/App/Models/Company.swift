//
//  Company.swift
//  App
//
//  Created by Kasper Welner on 07/10/2017.
//

import Vapor
import FluentProvider

// sourcery: model
final class Company: Model {
    
// sourcery: unique         = true
    var name:String
    
///Use this invitation code when creating a user to make the user an owner of this company
// sourcery: unique         = true
    var ownerInvitationCode:String

///Use this invitation code when creating a user to make the user a regular user of this company
// sourcery: unique         = true
    var userInvitationCode:String
    
// sourcery:inline:auto:Company.Models
    let storage = Storage()

    internal init(
        name: String,
        ownerInvitationCode: String,
        userInvitationCode: String
    ) {
        self.name = name
        self.ownerInvitationCode = ownerInvitationCode
        self.userInvitationCode = userInvitationCode
    }
// sourcery:end
}

extension Company {
    var users:Children<Company, User> {
        return children()
    }
    
    var employees:Children<Company, Employee> {
        return children()
    }
    
    var offers:Children<Company, Offer> {
        return children()
    }
}

extension Company {
    internal func makeJSONWithUsers() throws -> JSON {
        var json = try makeJSON()
        
        try json.set("users", users.all().makeJSON())
        try json.set("employees", employees.all().makeJSON())
        
        return json
    }
}
