// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Employee: RowConvertible {
    // MARK: - RowConvertible (Employee)
    convenience internal init (row: Row) throws {
        try self.init(
            firstName: row.get(DatabaseKeys.firstName),
            lastName: row.get(DatabaseKeys.lastName),
            title: row.get(DatabaseKeys.title),
            monthlySalary: row.get(DatabaseKeys.monthlySalary),
            hoursPerWeek: row.get(DatabaseKeys.hoursPerWeek),
            pensionStartDate: row.get(DatabaseKeys.pensionStartDate),
            pensionCustomPercent: row.get(DatabaseKeys.pensionCustomPercent),
            hasPension: row.get(DatabaseKeys.hasPension),
            companyId: row.get(DatabaseKeys.companyId),
            userId: row.get(DatabaseKeys.userId)
        )
    }

    internal func makeRow() throws -> Row {
        var row = Row()

        try row.set(DatabaseKeys.firstName, firstName)
        try row.set(DatabaseKeys.lastName, lastName)
        try row.set(DatabaseKeys.title, title)
        try row.set(DatabaseKeys.monthlySalary, monthlySalary)
        try row.set(DatabaseKeys.hoursPerWeek, hoursPerWeek)
        try row.set(DatabaseKeys.pensionStartDate, pensionStartDate)
        try row.set(DatabaseKeys.pensionCustomPercent, pensionCustomPercent)
        try row.set(DatabaseKeys.hasPension, hasPension)
        try row.set(DatabaseKeys.companyId, companyId)
        try row.set(DatabaseKeys.userId, userId)

        return row
    }
}
