// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Employee: Preparation {
    internal enum DatabaseKeys {
        static let id = Employee.idKey
        static let firstName = "firstName"
        static let lastName = "lastName"
        static let title = "title"
        static let monthlySalary = "monthlySalary"
        static let hoursPerWeek = "hoursPerWeek"
        static let pensionStartDate = "pensionStartDate"
        static let pensionCustomPercent = "pensionCustomPercent"
        static let hasPension = "hasPension"
        static let companyId = "companyId"
        static let userId = "userId"
    }

    // MARK: - Preparations (Employee)
    internal static func prepare(_ database: Database) throws {
        try database.create(self) {
            $0.id()
            $0.string(DatabaseKeys.firstName)
            $0.string(DatabaseKeys.lastName)
            $0.string(DatabaseKeys.title)
            $0.int(DatabaseKeys.monthlySalary)
            $0.double(DatabaseKeys.hoursPerWeek)
            $0.date(DatabaseKeys.pensionStartDate)
            $0.double(DatabaseKeys.pensionCustomPercent, optional: true)
            $0.bool(DatabaseKeys.hasPension)
            $0.foreignId(for: Company.self, optional: false, unique: false, foreignIdKey: DatabaseKeys.companyId)
            $0.foreignId(for: User.self, optional: false, unique: false, foreignIdKey: DatabaseKeys.userId)
        }

    }

    internal static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}
