// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Employee: JSONConvertible {
    internal enum JSONKeys: String {
        case firstName
        case lastName
        case title
        case monthlySalary
        case hoursPerWeek
        case pensionStartDate
        case pensionCustomPercent
        case hasPension
        case companyId
        case userId
    }

    // MARK: - JSONConvertible (Employee)
    internal convenience init(json: JSON) throws {
        try self.init(
            firstName: json.get(JSONKeys.firstName.rawValue),
            lastName: json.get(JSONKeys.lastName.rawValue),
            title: json.get(JSONKeys.title.rawValue),
            monthlySalary: json.get(JSONKeys.monthlySalary.rawValue),
            hoursPerWeek: json.get(JSONKeys.hoursPerWeek.rawValue),
            pensionStartDate: json.get(JSONKeys.pensionStartDate.rawValue),
            pensionCustomPercent: json.get(JSONKeys.pensionCustomPercent.rawValue),
            hasPension: json.get(JSONKeys.hasPension.rawValue),
            companyId: json.get(JSONKeys.companyId.rawValue),
            userId: json.get(JSONKeys.userId.rawValue)
        )
    }

    internal func makeJSON() throws -> JSON {
        var json = JSON()

        try json.set(Employee.idKey, id)
        try json.set(JSONKeys.firstName.rawValue, firstName)
        try json.set(JSONKeys.lastName.rawValue, lastName)
        try json.set(JSONKeys.title.rawValue, title)
        try json.set(JSONKeys.monthlySalary.rawValue, monthlySalary)
        try json.set(JSONKeys.hoursPerWeek.rawValue, hoursPerWeek)
        try json.set(JSONKeys.pensionStartDate.rawValue, pensionStartDate)
        try json.set(JSONKeys.pensionCustomPercent.rawValue, pensionCustomPercent)
        try json.set(JSONKeys.hasPension.rawValue, hasPension)
        try json.set(JSONKeys.companyId.rawValue, companyId)
        try json.set(JSONKeys.userId.rawValue, userId)

        return json
    }
}

extension Employee: ResponseRepresentable {}
