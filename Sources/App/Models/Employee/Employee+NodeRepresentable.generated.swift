// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Employee: NodeRepresentable {
    internal enum NodeKeys: String {
        case firstName
        case lastName
        case title
        case monthlySalary
        case hoursPerWeek
        case pensionStartDate
        case pensionCustomPercent
        case hasPension
        case companyId
        case userId
    }

    // MARK: - NodeRepresentable (Employee)
    func makeNode(in context: Context?) throws -> Node {
        var node = Node([:])

        try node.set(Employee.idKey, id)
        try node.set(NodeKeys.firstName.rawValue, firstName)
        try node.set(NodeKeys.lastName.rawValue, lastName)
        try node.set(NodeKeys.title.rawValue, title)
        try node.set(NodeKeys.monthlySalary.rawValue, monthlySalary)
        try node.set(NodeKeys.hoursPerWeek.rawValue, hoursPerWeek)
        try node.set(NodeKeys.pensionStartDate.rawValue, pensionStartDate)
        try node.set(NodeKeys.pensionCustomPercent.rawValue, pensionCustomPercent)
        try node.set(NodeKeys.hasPension.rawValue, hasPension)
        try node.set(NodeKeys.companyId.rawValue, companyId)
        try node.set(NodeKeys.userId.rawValue, userId)
        try node.set(Employee.createdAtKey, createdAt)
        try node.set(Employee.updatedAtKey, updatedAt)

        return node
    }
}
