// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Expense: Preparation {
    internal enum DatabaseKeys {
        static let id = Expense.idKey
        static let title = "title"
        static let cost = "cost"
        static let offerId = "offerId"
    }

    // MARK: - Preparations (Expense)
    internal static func prepare(_ database: Database) throws {
        try database.create(self) {
            $0.id()
            $0.string(DatabaseKeys.title)
            $0.int(DatabaseKeys.cost)
            $0.foreignId(for: Offer.self, optional: false, unique: false, foreignIdKey: DatabaseKeys.offerId)
        }

    }

    internal static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}
