// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Expense: NodeRepresentable {
    internal enum NodeKeys: String {
        case title
        case cost
        case offerId
    }

    // MARK: - NodeRepresentable (Expense)
    func makeNode(in context: Context?) throws -> Node {
        var node = Node([:])

        try node.set(Expense.idKey, id)
        try node.set(NodeKeys.title.rawValue, title)
        try node.set(NodeKeys.cost.rawValue, cost)
        try node.set(NodeKeys.offerId.rawValue, offerId)

        return node
    }
}
