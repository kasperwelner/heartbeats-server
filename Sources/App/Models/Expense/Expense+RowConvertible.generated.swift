// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Expense: RowConvertible {
    // MARK: - RowConvertible (Expense)
    convenience internal init (row: Row) throws {
        try self.init(
            title: row.get(DatabaseKeys.title),
            cost: row.get(DatabaseKeys.cost),
            offerId: row.get(DatabaseKeys.offerId)
        )
    }

    internal func makeRow() throws -> Row {
        var row = Row()

        try row.set(DatabaseKeys.title, title)
        try row.set(DatabaseKeys.cost, cost)
        try row.set(DatabaseKeys.offerId, offerId)

        return row
    }
}
