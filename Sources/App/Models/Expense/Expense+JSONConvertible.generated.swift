// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Expense: JSONConvertible {
    internal enum JSONKeys: String {
        case title
        case cost
        case offerId
    }

    // MARK: - JSONConvertible (Expense)
    internal convenience init(json: JSON) throws {
        try self.init(
            title: json.get(JSONKeys.title.rawValue),
            cost: json.get(JSONKeys.cost.rawValue),
            offerId: json.get(JSONKeys.offerId.rawValue)
        )
    }

    internal func makeJSON() throws -> JSON {
        var json = JSON()

        try json.set(Expense.idKey, id)
        try json.set(JSONKeys.title.rawValue, title)
        try json.set(JSONKeys.cost.rawValue, cost)
        try json.set(JSONKeys.offerId.rawValue, offerId)

        return json
    }
}

extension Expense: ResponseRepresentable {}
