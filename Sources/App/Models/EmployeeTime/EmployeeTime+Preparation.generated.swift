// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension EmployeeTime: Preparation {
    internal enum DatabaseKeys {
        static let id = EmployeeTime.idKey
        static let days = "days"
        static let employeeId = "employeeId"
        static let offerId = "offerId"
    }

    // MARK: - Preparations (EmployeeTime)
    internal static func prepare(_ database: Database) throws {
        try database.create(self) {
            $0.id()
            $0.double(DatabaseKeys.days)
            $0.foreignId(for: Employee.self, optional: false, unique: false, foreignIdKey: DatabaseKeys.employeeId)
            $0.foreignId(for: Offer.self, optional: false, unique: false, foreignIdKey: DatabaseKeys.offerId)
        }

    }

    internal static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}
