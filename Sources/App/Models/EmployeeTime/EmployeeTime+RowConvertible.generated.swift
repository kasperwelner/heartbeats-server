// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension EmployeeTime: RowConvertible {
    // MARK: - RowConvertible (EmployeeTime)
    convenience internal init (row: Row) throws {
        try self.init(
            days: row.get(DatabaseKeys.days),
            employeeId: row.get(DatabaseKeys.employeeId),
            offerId: row.get(DatabaseKeys.offerId)
        )
    }

    internal func makeRow() throws -> Row {
        var row = Row()

        try row.set(DatabaseKeys.days, days)
        try row.set(DatabaseKeys.employeeId, employeeId)
        try row.set(DatabaseKeys.offerId, offerId)

        return row
    }
}
