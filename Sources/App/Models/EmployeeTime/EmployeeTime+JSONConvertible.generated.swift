// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension EmployeeTime: JSONConvertible {
    internal enum JSONKeys: String {
        case days
        case employeeId
        case offerId
    }

    // MARK: - JSONConvertible (EmployeeTime)
    internal convenience init(json: JSON) throws {
        try self.init(
            days: json.get(JSONKeys.days.rawValue),
            employeeId: json.get(JSONKeys.employeeId.rawValue),
            offerId: json.get(JSONKeys.offerId.rawValue)
        )
    }

    internal func makeJSON() throws -> JSON {
        var json = JSON()

        try json.set(EmployeeTime.idKey, id)
        try json.set(JSONKeys.days.rawValue, days)
        try json.set(JSONKeys.employeeId.rawValue, employeeId)
        try json.set(JSONKeys.offerId.rawValue, offerId)

        return json
    }
}

extension EmployeeTime: ResponseRepresentable {}
