// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension EmployeeTime: NodeRepresentable {
    internal enum NodeKeys: String {
        case days
        case employeeId
        case offerId
    }

    // MARK: - NodeRepresentable (EmployeeTime)
    func makeNode(in context: Context?) throws -> Node {
        var node = Node([:])

        try node.set(EmployeeTime.idKey, id)
        try node.set(NodeKeys.days.rawValue, days)
        try node.set(NodeKeys.employeeId.rawValue, employeeId)
        try node.set(NodeKeys.offerId.rawValue, offerId)

        return node
    }
}
