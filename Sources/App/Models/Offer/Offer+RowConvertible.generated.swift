// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Offer: RowConvertible {
    // MARK: - RowConvertible (Offer)
    convenience internal init (row: Row) throws {
        try self.init(
            userId: row.get(DatabaseKeys.userId),
            companyId: row.get(DatabaseKeys.companyId),
            totalTenderAmountOverride: row.get(DatabaseKeys.totalTenderAmountOverride)
        )
    }

    internal func makeRow() throws -> Row {
        var row = Row()

        try row.set(DatabaseKeys.userId, userId)
        try row.set(DatabaseKeys.companyId, companyId)
        try row.set(DatabaseKeys.totalTenderAmountOverride, totalTenderAmountOverride)

        return row
    }
}
