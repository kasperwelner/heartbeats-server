// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Offer: JSONConvertible {
    internal enum JSONKeys: String {
        case userId
        case companyId
        case totalTenderAmountOverride
    }

    // MARK: - JSONConvertible (Offer)
    internal convenience init(json: JSON) throws {
        try self.init(
            userId: json.get(JSONKeys.userId.rawValue),
            companyId: json.get(JSONKeys.companyId.rawValue),
            totalTenderAmountOverride: json.get(JSONKeys.totalTenderAmountOverride.rawValue)
        )
    }

    internal func makeJSON() throws -> JSON {
        var json = JSON()

        try json.set(Offer.idKey, id)
        try json.set(JSONKeys.userId.rawValue, userId)
        try json.set(JSONKeys.companyId.rawValue, companyId)
        try json.set(JSONKeys.totalTenderAmountOverride.rawValue, totalTenderAmountOverride)

        return json
    }
}

extension Offer: ResponseRepresentable {}
