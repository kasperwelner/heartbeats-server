// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Offer: Preparation {
    internal enum DatabaseKeys {
        static let id = Offer.idKey
        static let userId = "userId"
        static let companyId = "companyId"
        static let totalTenderAmountOverride = "totalTenderAmountOverride"
    }

    // MARK: - Preparations (Offer)
    internal static func prepare(_ database: Database) throws {
        try database.create(self) {
            $0.id()
            $0.foreignId(for: User.self, optional: false, unique: false, foreignIdKey: DatabaseKeys.userId)
            $0.foreignId(for: Company.self, optional: false, unique: false, foreignIdKey: DatabaseKeys.companyId)
            $0.double(DatabaseKeys.totalTenderAmountOverride, optional: true)
        }

    }

    internal static func revert(_ database: Database) throws {
        try database.delete(self)
    }
}
