// Generated using Sourcery 0.9.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT


import Vapor
import Fluent

extension Offer: NodeRepresentable {
    internal enum NodeKeys: String {
        case userId
        case companyId
        case totalTenderAmountOverride
    }

    // MARK: - NodeRepresentable (Offer)
    func makeNode(in context: Context?) throws -> Node {
        var node = Node([:])

        try node.set(Offer.idKey, id)
        try node.set(NodeKeys.userId.rawValue, userId)
        try node.set(NodeKeys.companyId.rawValue, companyId)
        try node.set(NodeKeys.totalTenderAmountOverride.rawValue, totalTenderAmountOverride)
        try node.set(Offer.createdAtKey, createdAt)
        try node.set(Offer.updatedAtKey, updatedAt)

        return node
    }
}
