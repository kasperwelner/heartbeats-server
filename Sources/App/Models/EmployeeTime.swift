//
//  EmployeeTime.swift
//  App
//
//  Created by Kasper Welner on 07/10/2017.
//

import Foundation
import Vapor
import FluentProvider

// sourcery: model
final class EmployeeTime: Model {
    var days: Double

// sourcery: foreignTable   = Employee
// sourcery: unique         = false
// sourcery: preparation    = foreignId
    let employeeId:Identifier

// sourcery: preparation    = foreignId, unique = false, foreignTable = Offer
    let offerId:Identifier
    
// sourcery:inline:auto:EmployeeTime.Models
    let storage = Storage()

    internal init(
        days: Double,
        employeeId: Identifier,
        offerId: Identifier
    ) {
        self.days = days
        self.employeeId = employeeId
        self.offerId = offerId
    }
// sourcery:end
}

extension EmployeeTime {
    var employee: Parent<EmployeeTime, Employee> {
        return parent(id: employeeId)
    }
}

extension EmployeeTime {
    func unwrapJSON() throws -> JSON {
        var json = try makeJSON()
        json[JSONKeys.offerId.rawValue] = nil
        json[JSONKeys.employeeId.rawValue] = nil
        json["Employee"] = try employee.get()?.makeJSON()
        return json
    }
}
