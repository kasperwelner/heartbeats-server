//
//  Request+JSONBody.swift
//  App
//
//  Created by Kasper Welner on 04/10/2017.
//

import HTTP
import Vapor

extension Request {
    func JSONData() throws -> JSON {
        guard contentType == "application/json", let json = json else { throw Abort.init(Status.unsupportedMediaType, reason: "application/JSON is the only payload content type supported") }
        return json
    }
}
