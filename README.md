
##Heartbeats API
This is the Vapor app for the API.

####To import in postman:
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/35644037075156bb481a)

####To import in postman (with environment):
[![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/35644037075156bb481a#?env%5BProduction%5D=W3siZW5hYmxlZCI6dHJ1ZSwia2V5IjoiYmFzZXVybCIsInZhbHVlIjoiaHR0cDovL2hlYXJ0YmVhdHMudmFwb3IuY2xvdWQvYXBpIiwidHlwZSI6InRleHQifSx7ImVuYWJsZWQiOnRydWUsImtleSI6Im5hbWUiLCJ2YWx1ZSI6Ikthc3BlciIsInR5cGUiOiJ0ZXh0In0seyJlbmFibGVkIjp0cnVlLCJrZXkiOiJlbWFpbCIsInZhbHVlIjoia2FzcGVyQHRlc3QuY29tIiwidHlwZSI6InRleHQifSx7ImVuYWJsZWQiOnRydWUsImtleSI6InBhc3N3b3JkIiwidmFsdWUiOiJqdXN0YXBhc3N3b3JkIiwidHlwZSI6InRleHQifSx7ImVuYWJsZWQiOnRydWUsImtleSI6InRva2VuIiwidmFsdWUiOiJybk1UTHhVZS9mZXAzZ2s0WHp1UmlBPT0iLCJ0eXBlIjoidGV4dCJ9XQ==)
